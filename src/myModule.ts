import fs from 'fs/promises';
import { getData, save, task } from './db/data.js';
const uid = () => Math.random().toString(36).substring(2);

export async function create(str: string): Promise<void> {
    let data = await getData();
    let task: task = {
        id: uid(),
        name: str,
        isComleted: false
    }
    data.push(task);
    save(data);
}

export async function read(): Promise<task[]> {
    return getData();
}
export async function update(id: string): Promise<void> {
    let data = await getData();
    for(let task of data){
        if(task.id === id){
            task.isComleted = !(task.isComleted);
        }
    }
    save(data);
}

export async function delete_task(id: string): Promise<void> {
    let data = await getData();
    data = data.filter((task) => task.id!==id);
    save(data);
}
export async function open_tasks(): Promise<task[]>{
    let data = await getData();
    let open  = data.filter((task) => task.isComleted===false);
    return open;
}
export async function completed():  Promise<task[]>{
    let data = await getData();
    let completed_task  = data.filter((task) => task.isComleted===true);
    return completed_task;
}
export async function all(): Promise<task[]> {
    let data =  getData();
    return data;
}
export async function remove_complited(id: string):Promise<task[]>{
    let data = await getData();
     data = await open_tasks();
     await fs.writeFile('./todo.json', JSON.stringify(data, null, 2));
     return data;
}

export async function options(id: string): Promise<void> {
    console.log("isnt correct option");
}