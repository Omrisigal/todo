import * as todo_func from "./myModule.js";
import {start} from './db/data.js';

 async function init(){    
    await start();
    var myArgs = process.argv.slice(2);
        switch (myArgs[0]) {
            case 'create':
               await todo_func.create(myArgs[1]);
                break;
            case 'read':
            console.table(await todo_func.read());
                break;
            case 'update':
                await todo_func.update(myArgs[1]);
                break;
            case 'delete_t':
                await todo_func.delete_task(myArgs[1]);
                break;
            case 'all':
                console.log('here');
                console.table(await todo_func.read());
                break;
            case 'completed':
                await todo_func.completed();
                break;
            case 'open_tasks':
                await todo_func.open_tasks();
                break;
            case 'remove_complited':
                await todo_func.remove_complited(myArgs[1]);
                break;
            default:
                await todo_func.options(myArgs[1]);
            }
}
init();

